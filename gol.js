const taille = 20;
var grid = [];
var canvas = document.getElementsByTagName("canvas")[0];
var ctx = canvas.getContext('2d');
var flag = true;
var arret = true;

function main() {
    if (arret == true){
        arret = false;
    } else {
        flag = true;
    }
    if (flag == true){
        document.getElementsByTagName("input")[0].setAttribute("value", "Stop");
        document.getElementsByTagName("input")[0].setAttribute("onclick", "stop();");
        generation();
        setTimeout(main, 100);
    }
}

function stop(){
    flag = false;
    arret = true;
    document.getElementsByTagName("input")[0].setAttribute("value", "Lancer");
    document.getElementsByTagName("input")[0].setAttribute("onclick", "main();");
}

function reset() {
    stop();
    genGrid();
}

function generation(){
    gridTmp = [];
    gridTmp = initGridTmp(gridTmp);

    for (var i = 0; i < grid.length; i++){
        for (var j = 0; j < grid[i].length; j++) {
            if (grid[i][j] == 1){
                if (nombreDeCellVivanteAutour(i, j) < 2){
                    gridTmp[i][j] = 0;
                } else if (nombreDeCellVivanteAutour(i, j) == 2 || nombreDeCellVivanteAutour(i, j) == 3){
                    gridTmp[i][j] = 1;
                } else if (nombreDeCellVivanteAutour(i, j) > 3){
                    gridTmp[i][j] = 0;
                }
            } else {
                if (nombreDeCellVivanteAutour(i, j) == 3){
                    gridTmp[i][j] = 1;
                }
            }
        }
    }

    for (var i = 0; i < grid.length; i++){
        for (var j = 0; j < grid[i].length; j++) {
            grid[i][j] = gridTmp[i][j];
        }
    }

    for (var i = 0; i < grid.length; i++){
        for (var j = 0; j < grid[i].length; j++) {
            if (grid[i][j] == 1){
                drawFilledSquare(i*taille, j*taille);
            } else {
                drawEmptySquare(i*taille, j*taille);
            }
        }
    }
}

function initGridTmp(gridTmp) {
    for (var i = 0; i < grid.length; i++){
        gridTmp.push([]);
        for (var j = 0; j < grid[i].length; j++) {
            gridTmp[i].push(0);
        }
    }
    return gridTmp;
}

function nombreDeCellVivanteAutour(i, j) {
    let nombreDeVivantes = 0;
    // console.log(nombreDeVivantes);
    if (j+1 < grid[i].length){
        nombreDeVivantes += grid[i][j+1];
        // console.log(grid[i][j+1]);
        // console.log(nombreDeVivantes);
    }
    if (i+1 < grid.length){
        nombreDeVivantes += grid[i+1][j];
        // console.log(nombreDeVivantes);
    }
    if (j-1 >= 0){
        nombreDeVivantes += grid[i][j-1];
        // console.log(nombreDeVivantes);
    }
    if (i-1 >= 0){
        nombreDeVivantes += grid[i-1][j];
        // console.log(nombreDeVivantes);
    }
    if (j-1 >= 0 && i+1 < grid.length){
        nombreDeVivantes += grid[i+1][j-1];
        // console.log(nombreDeVivantes);
    }
    if (j+1 < grid[i].length && i-1 >= 0){
        nombreDeVivantes += grid[i-1][j+1];
        // console.log(nombreDeVivantes);
    }
    if (j+1 < grid[i].length && i+1 < grid.length){
        nombreDeVivantes += grid[i+1][j+1];
        // console.log(nombreDeVivantes);
    }
    if (j-1 >= 0 && i-1 >= 0){
        nombreDeVivantes += grid[i-1][j-1];
        // console.log(nombreDeVivantes);
    }
    // console.log(nombreDeVivantes);
    return nombreDeVivantes;
}

function genGrid() {
    let largeur = 50*taille;
    let hauteur = 30*taille;
    if (document.getElementById("largeur").value){
        largeur = document.getElementById("largeur").value*taille;
    }
    if (document.getElementById("hauteur").value){
        hauteur = document.getElementById("hauteur").value*taille;
    }
    canvas.width = largeur;
    canvas.height = hauteur;
    canvas.addEventListener("click", addOrRemoveCellToTab, false);
    grid = [];
    for (var i = 0; i < largeur; i+=taille){
        grid.push([]);
        for (var j = 0; j < hauteur; j+=taille) {
            drawEmptySquare(i, j);
            grid[i/taille].push(0);
        }
    }
}

function drawEmptySquare(x, y){
    ctx.fillStyle = 'white';
    ctx.fillRect(x, y, taille, taille);
    ctx.strokeRect(x, y, taille, taille);
}

function drawFilledSquare(x, y){
    ctx.fillStyle = 'black';
    ctx.fillRect(x, y, taille, taille);
}

function addOrRemoveCellToTab(e) {
    let posX = e.clientX;
    let posY = e.clientY;
    let ind = convertPosToIndInTab(posX, posY);
    let i = ind[0];
    let j = ind[1];
    // console.log(i, j);
    if (grid[i][j] == 0){
        grid[i][j] = 1;
        drawFilledSquare(i*taille, j*taille);
    } else {
        grid[i][j] = 0;
        drawEmptySquare(i*taille, j*taille);
    }
}

function convertPosToIndInTab(x, y) {
    var i = Math.round(x / taille)-1;
    var j = Math.round(y / taille)-1;
    if (i < 0){
        i = 0;
    }
    if (j < 0){
        j = 0;
    }
    return [i, j];
}